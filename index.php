<?php

require('animal.php');
require('frog.php');
require('ape.php');
$sheep = new animal("shaun");
echo "Name = ".$sheep->name ."<br>";
echo "legs = ".$sheep->legs ."<br>";
echo "cold blooded = ".$sheep->cold_blooded ."<br>";
echo "<br>";

$kodok = new Frog("buduk");
echo "Name : ".$kodok->name ."<br>";
echo "legs : ".$kodok->legs ."<br>";
echo "cold blooded : ".$kodok->cold_blooded ."<br>Jump : ";
echo $kodok->Jump();
echo "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Name : ".$sungokong->name ."<br>";
echo "legs : ".$sungokong->legs ."<br>";
echo "cold blooded : ".$sungokong->cold_blooded ."<br>Yell : ";
echo $sungokong->Yell();

?>
